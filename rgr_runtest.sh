#! /bin/bash -e

if [ "$1" == "--all" ] 
then

	for i in `ls tests` 
	do

		echo "Running test $i"
		./opt_rgr/prog "tests/$i/input.sig" > "tests/$i/generated.txt"
	done

else
	echo "Running test $1"
	./opt_rgr/prog "$1/input.sig" > "$1/generated.txt"

fi

