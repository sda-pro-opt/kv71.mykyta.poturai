#include "Lexer.h"

#include <map>
#include <iostream>

//All forbidden by default
SymbolCategory SymbolCategories[128] = { forbidden };
std::map<std::string, int> reserved = {
  {"PROCEDURE", 401},
  {"BEGIN", 402},
  {"END", 403},
  {"RETURN", 404},
};


Lexer::Lexer(){

  SymbolCategories[8] = ws;
  SymbolCategories[9] = ws;
  SymbolCategories[10] = ws;
  SymbolCategories[13] = ws;
  SymbolCategories[32] = ws;
  for( int i = 'A'; i <= 'Z'; i++)
    SymbolCategories[i] = let;

  for( int i = '0'; i <= '9'; i++)
    SymbolCategories[i] = dig;
  SymbolCategories[','] = delim;
  SymbolCategories[';'] = delim;
  SymbolCategories['('] = bcom;
  SymbolCategories[')'] = delim;
  SymbolCategories['#'] = bstrange;
  okFlag = true;
  constCounter = 500;
  identCounter = 1000;
}

void Lexer::process(std::ifstream& input){
  col = 1;
  line = 1;
  this->input = &input;
  while(!input.eof()){
    current = input.get();
    if(current == -1)
      break;
    while(processChar()){}
      //processChar();//Next char already read
  }
}

void Lexer::processDigits(){
  std::string buf;
  buf.push_back(current);
  int start = col;
  iterCol();
  while(!input->eof()){
    current = input->get();
    switch(SymbolCategories[current]){
      case dig:
        buf.push_back(current);
        iterCol();
        continue;
      case ws:
      case delim:
        pushLexem(buf, pushConstant(buf), line, start);
        return;
      default:
        reportErr();
        return;
    }
  }
  pushLexem(buf, pushConstant(buf), line, col);
}

void Lexer::processLetters(){
  std::string buf;
  buf.push_back(current);
  int start = col;
  iterCol();
  while(!input->eof()){
    current = input->get();
    switch(SymbolCategories[current]){
      case dig:
      case let:
        buf.push_back(current);
        iterCol();
        continue;
      //forbidden:
        //reportErr();
        ////return;
      default:
        if(reserved.count(buf)){
          pushLexem(buf, reserved[buf], line, start);
        }else{
          pushLexem(buf, pushIdent(buf), line, start);
        }
        return;
    }
  }
  if(reserved.count(buf)){
    pushLexem(buf, reserved[buf], line, col);
  }else{
    pushLexem(buf, pushIdent(buf), line, col);
  }
}

int Lexer::processComment(){
  bool inComment = false;
  int comState = 0;
  char next = input->get();
  if (next == '*'){
    inComment= true;
  }else{
      pushLexem(std::string(1, current), current, line, col);
      iterCol();
      current = next;
      return 1;
  }
  iterCol();
  current = next;
  iterCol();

  while(!input->eof()){
    current = input->get();
    switch(current){
      case '*':
        comState = 1;
        break;
      case ')':
        if (comState == 1){
	    iterCol();
          return 0;
        }else{
          comState = 0;
	  break;
	  
        }
      default:
	comState = 0;
    }
    iterCol();
  }
  reportErrEof();
  return 0;
}

int Lexer::processStrange(){

	std::string buf;
	buf += current;
	int start = col;
	int state = 0;
	// 0 digits
	// 1 symbols

  while(!input->eof()){
    current = input->get();
    switch (current){
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	    case '0':
		    if (state == 0) {
			    buf += current;
		    }else{
			    reportErr();
		    }
		    break;
	    case '+':
	    case '-':
	    case '*':
	    case '/':
		    state=1;
		    buf += current;
		    break;
	    default:
		    if (state == 0) reportErr();
		pushLexem(buf, pushConstant(buf), line, start);
		    iterCol();
		    return 1;
    }



    iterCol();
  }
}

int Lexer::processChar(){
  switch(SymbolCategories[current]){
    case forbidden:
      reportErr();
      iterCol();
      return 0;
    case bstrange:

      processStrange();
      return 1;
   case ws:
      iterCol();
      return 0;
    case dig:
      processDigits();
      return 1;
    case let:
      processLetters();
      return 1;
    case bcom:
      return processComment();
    case delim:
      pushLexem(std::string(1, current), current, line, col);
      iterCol();
      return 0;
  }
  return 0;
}
