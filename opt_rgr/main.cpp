#include <iostream>
#include <fstream>
#include "Lexer.h"
#include "Syntaxer.h"



int main(int argc, char** argv){

  if(argc < 2){
    std::cout << "Error: no input file" << std::endl;
    return -1;
  }
  
  std::ifstream file;
  file.open(argv[1]);

  Lexer lex;
  lex.process(file);
  lex.printData();
  if(!lex.isOk()){
	  for(auto err: lex.getErrors()){
      std::cout << err << std::endl;
    }
  }


  tree_t *tree = synax_analyze(&lex.data);
  if(tree)
	  print_tree(tree, 0);


  return 0;
}
