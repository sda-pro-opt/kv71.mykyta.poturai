#pragma once
#include "Lexer.h"


typedef enum tree_type {
	TERMINAL,
	SIGNAL_PROGRAM,
	PROGRAM,
	PROCEDURE_IDENTIFIER,
	PARAMETERS_LIST,
	BLOCK,
	STATEMENTS_LIST,
	DECLARATIONS,
	PROCEDURE_DECLARATIONS,
	PROCEDURE,
	VARIABLE_IDENTIFIER,
	IDENTIFIERS_LIST,
	ACTUAL_ARGUMENTS,
	UNSIGNED_INTEGER,
	ACTUAL_ARGUMENTS_LIST,
	IDENTIFIER,
	STRING,
	EMPTY,
	STATEMENT,
	TREE_TYPES_COUNT
} tree_type_t;


typedef struct tree {
	struct tree *children[255];
	int num_children;
	char *label;
	tree_type_t tt;
} tree_t;


tree_t *synax_analyze(LexerData*);
void print_tree(tree_t *tree, int depth);

