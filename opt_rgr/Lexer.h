#pragma once

#include <vector>
#include <string>
#include <fstream>

typedef struct Lexem {
  std::string value;
  int id;
  int line;
  int col;
} Lexem;

typedef struct Constant {
  std::string value;
  int id;
} Constant;

typedef struct Identifier {
  std::string value;
  int id;
} Identifier;

typedef struct LexerData {
  std::vector<Lexem> lexems;
  std::vector<Constant> constants;
  std::vector<Identifier> identifiers;

} LexerData;

typedef enum SymbolCategory {
  forbidden,
  let,
  dig,
  delim,
  ws,
  bcom,
  bstrange,
} SymbolCategory;

class Lexer{

    bool okFlag;
    unsigned int line;
    unsigned int col;
    int constCounter;
    int identCounter;
    char current;
    std::ifstream* input;
    std::vector<std::string> errors;

    int processChar();
    void processDigits();
    void processLetters();
    int processComment();
    int processStrange();

    //Helpers
    int pushConstant(std::string);
    int pushIdent(std::string);
    void pushLexem(std::string, int, int,int);

    void iterCol();
    void reportErr();
    void reportErrEof();

  public:
    Lexer();
    LexerData data;
    void process(std::ifstream&);
    bool isOk(){return okFlag;};
    LexerData getData(){return data;};
    std::vector<std::string> getErrors(){return errors;};
    void printData();

};
