#include "Lexer.h"
#include <iostream>
#include <sstream>
#include <iomanip>


void Lexer::reportErrEof(){
  okFlag = false;
  std::stringstream ss;
  ss << "Lexer: Error(Line " << line <<", Column " << col << "): Unexpected EOF";
  errors.push_back(ss.str());
}
void Lexer::reportErr(){
  okFlag = false;
  std::stringstream ss;
  ss << "Lexer: Error(Line " << line <<", Column " << col << "): Illegal symbol '"<< current <<"'";
  errors.push_back(ss.str());
}

void Lexer::iterCol(){
  //std::cout << "Lexer: (Line " << line <<", Column " << col << "): '"<< (int)current <<"'" << std::endl;
  col++;
  if(current == '\n'){
    line++;
    col = 1;
  }
}

int Lexer::pushConstant(std::string cons){
  for( auto& c: data.constants){
    if(c.value == cons){
      return c.id;
    }
  }
  data.constants.push_back({.value = cons, .id = ++constCounter});
  return constCounter;
};

int Lexer::pushIdent(std::string cons){
  for( auto& c: data.identifiers){
    if(c.value == cons){
      return c.id;
    }
  }
  data.identifiers.push_back({.value = cons, .id = ++identCounter});
  return identCounter;
};

void Lexer::pushLexem(std::string value, int id, int line, int col){
  data.lexems.push_back({.value = value, .id = id, .line = line, .col = col});
}

void Lexer::printData(){
  for(int i = 0; i < data.lexems.size(); i++){
    std::cout << std::setw(3) << data.lexems[i].line
      << " "
      << std::setw(3) << data.lexems[i].col
      << " "
      << std::setw(4) << data.lexems[i].id 
      << " "
      << data.lexems[i].value
      << std::endl;
  }

  std::cout << "Constants:" << std::endl;
  for(auto cons: data.constants){
    std::cout << "\t" << std::setw(4) << cons.id 
      << " "
      << cons.value
      << std::endl;
  }
  std::cout << "Identifiers:" << std::endl;
  for(auto ident: data.identifiers){
    std::cout << "\t" << std::setw(4) << ident.id 
      << " "
      << ident.value
      << std::endl;
  }

}


