#include "Syntaxer.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


const char *labels[TREE_TYPES_COUNT] {
	"TERMINAL",
	"SIGNAL_PROGRAM",
	"PROGRAM",
	"PROCEDURE_IDENTIFIER",
	"PARAMETERS_LIST",
	"BLOCK",
	"STATEMENTS_LIST",
	"DECLARATIONS",
	"PROCEDURE_DECLARATIONS",
	"PROCEDURE",
	"VARIABLE_IDENTIFIER",
	"IDENTIFIERS_LIST",
	"ACTUAL_ARGUMENTS",
	"UNSIGNED_INTEGER",
	"ACTUAL_ARGUMENTS_LIST",
	"IDENTIFIER",
	"STRING",
	"EMPTY",
	"STATEMENT",
};

tree *alloc_tree(){
	return (tree*)calloc(sizeof(tree), 1);
}


#define ISERR() if(is_err) {return 0;} 


std::vector<Lexem> lexems;
int ind;
int is_err;

void pr_err(Lexem lex, std::string format, ...){
	char *ret;
	va_list args;
	ind = 0;
	va_start(args, format);
	vasprintf(&ret, format.c_str(), args);
	va_end(args);
	printf("Syntaxer: Error(Line %d, Column %d): %s\n",lex.line, lex.col, ret);
	is_err = 1;
}

tree *string_node(int id, const char *label){

	tree_t *tree = alloc_tree();
	tree->num_children = 0;
	tree->tt = TERMINAL;
	asprintf(&tree->label, "%d %s", id, label);
	ind++;
	return tree;
}

tree *empty(){
	tree_t *tree = alloc_tree();
	tree->num_children = 0;
	tree->tt = EMPTY;
	return tree;
}

tree *identifier(){
	tree_t *tree = alloc_tree();
	tree->tt = IDENTIFIER;
	tree->num_children = 1;
	if( lexems[ind].id <= 1000 ){
		pr_err(lexems[ind], "Expected identifier but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());
	return tree;
}

tree *variable_identifier(){
	tree_t *tree = alloc_tree();
	tree->tt = VARIABLE_IDENTIFIER;
	tree->num_children = 1;
	tree->children[0] = identifier();
	ISERR();
	return tree;
}

tree *identifiers_list(){
	tree_t *tree = alloc_tree();
	tree->tt = IDENTIFIERS_LIST;
	if(lexems[ind].id != ','){
		tree->num_children =1;
		tree->children[0] = empty();
		return tree;
	}

	tree->num_children = 3;
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = variable_identifier();
	ISERR();

	tree->children[2] = identifiers_list();
	ISERR();

	return tree;
}

tree *parameters_list(){
	tree_t *tree = alloc_tree();
	tree->tt = PARAMETERS_LIST;
	if (lexems[ind].id != '('){ 
		tree->num_children = 1;
		tree->children[0] = empty();
		return tree;
	}
	tree->num_children = 4;
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = variable_identifier();
	ISERR();

	tree->children[2] = identifiers_list();
	ISERR();

	if(lexems[ind].id != ')') {
		pr_err(lexems[ind], "Expected ')' but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	return tree;
}

tree *procedure_identifier(){
	tree_t *tree = alloc_tree();
	tree->tt = PROCEDURE_IDENTIFIER;
	tree->num_children = 1;
	tree->children[0] = identifier();
	ISERR();
	return tree;
}

tree *procedure(){
	if (lexems[ind].id != 401){ //PROCEDURE
		return empty();
	}

	tree_t *tree = alloc_tree();
	tree->tt = PROCEDURE;
	tree->num_children = 4;

	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = procedure_identifier();
	ISERR();

	tree->children[2] = parameters_list();
	ISERR();

	if (lexems[ind].id != ';'){
		pr_err(lexems[ind], "Expected ; but %s found", lexems[ind].value.c_str());
		return 0;
	}

	tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	return tree;
}

tree *procedure_declarations(){
	tree_t *tree = alloc_tree();
	tree->tt = PROCEDURE_DECLARATIONS;
	tree->num_children = 1;

	tree->children[0] = procedure();

	if(tree->children[0]->tt == EMPTY){
		return tree;
	}
	tree->num_children = 2;
	tree->children[1] = procedure_declarations();
	return tree;

}

tree *declarations(){
	tree_t *tree = alloc_tree();
	tree->tt = DECLARATIONS;
	tree->num_children = 1;
	tree->children[0] = procedure_declarations();
	ISERR();
	return tree;
}

tree *unsigned_integer(){
	if (!(lexems[ind].id > 500 && lexems[ind].id <= 1000 )){ 
		pr_err(lexems[ind], "Expected integer but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree_t *tree = alloc_tree();
	tree->tt = UNSIGNED_INTEGER;
	tree->num_children = 1;
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());
	return tree;
}

tree *actual_arguments_list(){
	tree_t *tree = alloc_tree();
	tree->tt = ACTUAL_ARGUMENTS_LIST;
	if(lexems[ind].id != ','){
		tree->num_children =1;
		tree->children[0] = empty();
		return tree;
	}

	tree->num_children = 3;
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = unsigned_integer();
	ISERR();

	tree->children[2] = actual_arguments_list();
	ISERR();

	return tree;
}

tree *actual_arguments(){
	tree_t *tree = alloc_tree();
	tree->tt = ACTUAL_ARGUMENTS;
	if (lexems[ind].id != '('){ 
		tree->num_children = 1;
		tree->children[0] = empty();
		return tree;
	}
	tree->num_children = 4;
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = unsigned_integer();
	ISERR();

	tree->children[2] = actual_arguments_list();
	ISERR();

	if(lexems[ind].id != ')') {
		pr_err(lexems[ind], "Expected ')' but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	return tree;
}

tree *statement(){
	tree_t *tree = alloc_tree();
	tree->tt = STATEMENT;
	if(lexems[ind].id == 404){// RETURN
		tree->num_children = 2;
		tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

		if(lexems[ind].id != ';') {
			pr_err(lexems[ind], "Expected ; but %s found", lexems[ind].value.c_str());
			return 0;
		}
		tree->children[1] = string_node(lexems[ind].id, lexems[ind].value.c_str());
		return tree;
	}else if(lexems[ind].id > 1000){
		tree->num_children = 3;
		tree->children[0] = procedure_identifier();
		ISERR();
		tree->children[1] = actual_arguments();
		ISERR();
		if(lexems[ind].id != ';') {
			pr_err(lexems[ind], "Expected ; but %s found", lexems[ind].value.c_str());
			return 0;
		}
		tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());
		return tree;
	}else{
		free(tree);
		return empty();
	}
}

tree *statements_list(){
	tree_t *tree = alloc_tree();
	tree->tt = STATEMENTS_LIST;
	tree->num_children = 1;

	tree->children[0] = statement();
	ISERR();
	if (tree->children[0]->tt == EMPTY){
		return tree;
	}
	tree->num_children = 2;
	tree->children[1] = statements_list();
	ISERR();

	return tree;
}

tree *block(){
	tree_t *tree = alloc_tree();
	tree->tt = BLOCK;
	tree->num_children = 4;

	tree->children[0] = declarations();
	ISERR();
	if (lexems[ind].id != 402){ //BEGIN
		pr_err(lexems[ind],"BEGIN expected but %s found",lexems[ind].value.c_str());
		return 0;
	}
	tree->children[1] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[2] = statements_list();
	ISERR();


	if (lexems[ind].id != 403){ //END
		pr_err(lexems[ind],"END expected but %s found",lexems[ind].value.c_str());
		return 0;
	}
	tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	return tree;
}

tree *program(){
	tree_t *tree = alloc_tree();
	tree->tt = PROGRAM;
	tree->num_children = 7;

	if(! (lexems[ind].id == 401)) { //PROCEDURE
		pr_err(lexems[ind], "Expected PROCEDURE but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree->children[0] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[1] = procedure_identifier();
	ISERR();

	tree->children[2] = parameters_list();
	ISERR();

	if(! (lexems[ind].id == ';')) { //PROCEDURE
		pr_err(lexems[ind], "Expected ; but %s found", lexems[ind].value.c_str());
		return 0;
	}
	tree->children[3] = string_node(lexems[ind].id, lexems[ind].value.c_str());

	tree->children[4] = block();
	ISERR();

	if(! (lexems[ind].id == ';')) { //PROCEDURE
		pr_err(lexems[ind], "Expected ; but %s found", lexems[ind].value.c_str());
		return 0;
	}


	return tree;
}

tree *synax_analyze(LexerData* data){
	lexems = data->lexems;
	ind = 0;
	is_err = 0;
	tree_t  *tree = alloc_tree();
	tree->num_children = 1;
	tree->tt = SIGNAL_PROGRAM;
	tree->children[0] = program();
	ISERR();
	return tree;

}


void print_tree(tree_t *tree, int depth = 0){
	const char *lbl;
	if (!tree)
		return;
	if (tree->tt == TERMINAL)
		lbl = tree->label;
	else
		lbl = labels[tree->tt];
	for(int i = 0; i < depth; i++){
		printf("|..");
	}
	printf("%s\n", lbl);
	for(int i = 0; i < tree->num_children; i++){
		print_tree(tree->children[i], depth+1);
	}
}
