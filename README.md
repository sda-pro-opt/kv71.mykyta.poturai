## Signal programming language translator (Lexical and Syntax analyzers)
### Task
Variant 13
```
<signal-program> --> <program>
<program> --> PROCEDURE <procedure-identifier><parameters-list>; <block> ;
<block> --> <declarations> BEGIN <statements-list> END
<declarations> --> <procedure-declarations>
<procedure-declarations> --> <procedure><procedure-declarations> | <empty>
<procedure> --> PROCEDURE <procedure-identifier><parameters-list> ;
<parameters-list> --> ( <variable-identifier><identifiers-list> ) | <empty>
<identifiers-list> --> , <variable-identifier><identifiers-list> | <empty>
<statements-list> --> <statement> <statements-list> | <empty>
<statement> --> <procedure-identifier><actual-arguments> ; | RETURN ;
<actual-arguments> --> ( <unsigned-integer> <actual-arguments-list> ) | <empty>
<actual-arguments-list> --> , <unsigned-integer> <actual-arguments-list> | <empty>
<variable-identifier> --> <identifier>
<procedure-identifier> --> <identifier>
<identifier> --> <letter><string>
<string> --> <letter><string> | <digit><string> | <empty>
<unsigned-integer> --> <digit><digits-string>
<digits-string> --> <digit><digits-string> | <empty>
<digit> --> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
<letter> --> A | B | C | D | ... | Z
```


### Building
```
cd ./opt_rgr
make
```

Binary will be located at `opt_rgr/prog`

### Running
Binary accepts 1 argument - path to source file

All generated data is written to STDOUT(including errors)

### Tests
There are 9 tests located at tests/test1..9
For convenience, there is `rgr_runtest.sh` script

First argument is a path to the test folder `./rgr_runtest.sh tests/test1`
it feeds input file from test folder to the binary and redirects output to 
`generated.txt`

Also, first argument can be `--all` to run all tests

Test #1 is a true-test showing an example of valid AST generated

All other tests are designed to trigger various errors
